package com.informatica.controllers;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.CuentasEntity;
import com.informatica.services.CuentaService;

@RestController
@RequestMapping(value = "/cuenta")
public class CuentasController {
	private static final Logger logger = LoggerFactory.getLogger(CuentasController.class);
	
	@Autowired
	CuentaService cuentaService;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/crear", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String saveDataUsuario(@RequestBody CuentasEntity cuenta ) {
		
		logger.info("Datos cuenta: {}", cuenta);
		Map<String, Object> response = new HashMap<>();
		
		if (cuenta != null) {
			cuentaService.create(cuenta, response);
		} else {
			response.put("resultado", "datos incorrectos");
		}
		return new Gson().toJson(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(value = "/getCuentas/{usuarioId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String methodGet(@PathVariable("usuarioId") long usuarioId) {
		logger.info("Received: {}", usuarioId);
		Map<String, Object> response = new HashMap<>();
		if (usuarioId > 0) {
			cuentaService.getCuentas(usuarioId, response);
		}else {
			response.put("Error", "usuarioId es requerido");
		}
		return new Gson().toJson(response);
	}

}
