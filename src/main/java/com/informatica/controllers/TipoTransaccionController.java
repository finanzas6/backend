package com.informatica.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.TipoTransaccionesEntity;
import com.informatica.Repositories.interfaces.ITipoTransacciones;

@RestController
public class TipoTransaccionController {
	
	@Autowired
	ITipoTransacciones tipoTransaccion;

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(value = "/tipoTransaccion/get", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String getTipoTransaccion() {
		Map<String, Object> response = new HashMap<>();
		Iterable<TipoTransaccionesEntity> transaccion = tipoTransaccion.findAll();
		response.put("tipoTransaccion", transaccion);
		return new Gson().toJson(response);
	}
}
