package com.informatica.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.MedioPagoEntity;
import com.informatica.Repositories.interfaces.IMedioPagos;

@RestController
public class MedioPagoController {
	
	@Autowired
	IMedioPagos iMedioPagos;

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(value = "/mediosPagos/get", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String getCategorias() {
		Map<String, Object> response = new HashMap<>();
		Iterable<MedioPagoEntity> mediosPago = iMedioPagos.findAll();
		response.put("mediosPago", mediosPago);
		return new Gson().toJson(response);
	}
}
