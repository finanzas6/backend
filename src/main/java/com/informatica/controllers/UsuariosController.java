package com.informatica.controllers;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.UsuariosEntity;
import com.informatica.services.UsuariosServices;

@RestController
public class UsuariosController {

	private static final Logger logger = LoggerFactory.getLogger(UsuariosController.class);
	
	@Autowired
	UsuariosServices usuarioServices;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/login", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String login(@RequestBody UsuariosEntity usuario ) {
		
		logger.info("Received: {}", usuario);
		Map<String, Object> response = new HashMap<>();
		if (usuario != null) {
			usuarioServices.login(usuario, response);
		}else {
			response.put("Error", "username es requerido");
		}
		return new Gson().toJson(response);
	}
}
