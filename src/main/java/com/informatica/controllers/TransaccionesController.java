package com.informatica.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.TransaccionesEntity;
import com.informatica.Repositories.interfaces.ITransacciones;
import com.informatica.services.TransaccionesService;

@RestController
@RequestMapping(value = "/transaccion")
public class TransaccionesController {
	
	private static final Logger logger = LoggerFactory.getLogger(TransaccionesController.class);
	
	@Autowired
	TransaccionesService transaccionService;
	@Autowired
	ITransacciones iTransacciones;

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/crear", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String saveDataUsuario(@RequestBody TransaccionesEntity transaccion ) {
		
		logger.info("Datos transaccion: {}", transaccion);
		Map<String, Object> response = new HashMap<>();
		
		if (transaccion != null) {
			transaccionService.create(transaccion, response);
		} else {
			response.put("resultado", "datos incorrectos");
		}
		return new Gson().toJson(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(value = "/getTransaccion/{usuarioId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String methodGet(@PathVariable("usuarioId") long usuarioId) {
		logger.info("Received: {}", usuarioId);
		Map<String, Object> response = new HashMap<>();
		if (usuarioId > 0) {
			transaccionService.getTransaccionesByUsuario(usuarioId, response);
		}else {
			response.put("Error", "cuentaId es requerido");
		}
		return new Gson().toJson(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(value = "/reportTipo/{usuarioId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String reportTipo(@PathVariable("usuarioId") long usuarioId) {
		logger.info("Received: {}", usuarioId);
		Map<String, Object> response = new HashMap<>();
		
		if (usuarioId > 0) {
			List<Object[]> report = iTransacciones.getTransaccionesByTipo(usuarioId);
			
			if (report.size() > 0) {
				List<ReportResponse> reportResponseList = new ArrayList<>();
				
				for (Object[] objects : report) {
					ReportResponse reportResponse = new ReportResponse();
					reportResponse.setData(objects[0].toString());
					reportResponse.setTotal(Integer.valueOf(objects[1].toString()));
					reportResponseList.add(reportResponse);
				}
				response.put("resultado", reportResponseList);
			}
		}else {
			response.put("Error", "Usuario Id es requerido");
		}
		return new Gson().toJson(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(value = "/reportCategoria/{usuarioId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String reportCategoria(@PathVariable("usuarioId") long usuarioId) {
		logger.info("Received: {}", usuarioId);
		Map<String, Object> response = new HashMap<>();
		
		if (usuarioId > 0) {
			List<Object[]> report = iTransacciones.getTransaccionesByCategoria(usuarioId);
			
			if (report.size() > 0) {
				List<ReportResponse> reportResponseList = new ArrayList<>();
				
				for (Object[] objects : report) {
					ReportResponse reportResponse = new ReportResponse();
					reportResponse.setData(objects[0].toString());
					reportResponse.setTotal(Integer.valueOf(objects[1].toString()));
					reportResponseList.add(reportResponse);
				}
				response.put("resultado", reportResponseList);
			}
		}else {
			response.put("Error", "Usuario Id  es requerido");
		}
		return new Gson().toJson(response);
	}
}
