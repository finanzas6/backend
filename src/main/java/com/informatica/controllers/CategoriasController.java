package com.informatica.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.CategoriasEntity;
import com.informatica.Repositories.entities.CategoriasUsuarios;
import com.informatica.Repositories.interfaces.ICategorias;
import com.informatica.models.CategoriaRequest;
import com.informatica.models.DeleteRequest;
import com.informatica.services.CategoriasService;

@RestController
@RequestMapping(value = "/categoria")
public class CategoriasController {

	private static final Logger logger = LoggerFactory.getLogger(CategoriasController.class);
	
	@Autowired
	CategoriasService categoriasService;
	@Autowired
	ICategorias iCategorias;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/crear", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String saveCategoria(@RequestBody CategoriaRequest categoria ) {
		
		logger.info("Datos nueva categoria: {}", categoria);
		Map<String, Object> response = new HashMap<>();
		
		if (categoria != null) {
			categoriasService.create(categoria, response);
		} else {
			response.put("resultado", "datos incorrectos");
		}
		return new Gson().toJson(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/update", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String updateCategoria(@RequestBody CategoriasUsuarios categoriasUsuarios ) {
		
		logger.info("Datos categoria: {}", categoriasUsuarios);
		Map<String, Object> response = new HashMap<>();
		
		if (categoriasUsuarios != null) {
			categoriasService.update(categoriasUsuarios, response);
		} else {
			response.put("resultado", "datos incorrectos");
		}
		return new Gson().toJson(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/delete", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String deleteCategoria(@RequestBody DeleteRequest deleteRequest ) {
		
		logger.info("Datos categoria: {}", deleteRequest);
		Map<String, Object> response = new HashMap<>();
		
		if (deleteRequest != null) {
			categoriasService.delete(deleteRequest, response);
		} else {
			response.put("resultado", "datos incorrectos");
		}
		return new Gson().toJson(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(value = "/get/{usuarioId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String getCategorias(@PathVariable("usuarioId") int usuarioId) {
		logger.info("Received: {}", usuarioId);
		Map<String, Object> response = new HashMap<>();
		if (usuarioId > 0) {
			categoriasService.getCategorias(usuarioId, response);
		}else {
			response.put("Error", "usuarioId es requerido");
		}
		return new Gson().toJson(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(value = "/all", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String getCategoriasAll() {
		
		Map<String, Object> response = new HashMap<>();
		Iterable<CategoriasEntity> result = iCategorias.findAll();
		response.put("result", result);
		return new Gson().toJson(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(value = "/tipo/{tipo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String getCategoriasByTipo(@PathVariable("tipo") int tipo) {
		
		Map<String, Object> response = new HashMap<>();
		List<CategoriasEntity> result = iCategorias.getCategoriasByTipo(tipo);
		response.put("result", result);
		return new Gson().toJson(response);
	}
	
}
