package com.informatica.services;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.CuentasEntity;
import com.informatica.Repositories.interfaces.ICuentas;
import com.informatica.Repositories.interfaces.IUsuarios;

@Service
public class CuentaService {
	private static final Logger logger = LoggerFactory.getLogger(CuentaService.class);
	
	@Autowired
	ICuentas iCuentas;
	@Autowired
	IUsuarios iUsuarios;
	
	public void create(CuentasEntity cuenta, Map<String, Object> response) {
		try {
			if (cuenta.getUsuarioId() > 0 && userExists(cuenta.getUsuarioId())) {
				iCuentas.save(cuenta);
				response.put("resultado", "cuenta insertada correctamente");
			} else {
				response.put("error", new StringBuilder("UsuarioId: ").append(cuenta.getUsuarioId()).append(" invalido"));
				logger.info("Usuario Id invalido {}", cuenta.getUsuarioId());
			}
		} catch (Exception e) {
			logger.error("Exception: {}", e);
		}		
	}
	
	public String getCuentas(long usuarioId, Map<String, Object> response) {
		try {
			List<CuentasEntity> cuentas = iCuentas.findCuentasByUsuarioId(usuarioId);
			if (cuentas != null && !cuentas.isEmpty()) {
				response.put("cuentas", cuentas);
			} else {
				response.put("cuentas", null);
			}
		} catch (Exception e) {
			logger.error("Exception: {}", e);
		}
		return new Gson().toJson(response) ;
		
	}
	
	private boolean userExists(long id) {
		return iUsuarios.totalUsuariosById(id) > 0;
	}

}
