package com.informatica.services;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.UsuariosEntity;
import com.informatica.Repositories.interfaces.IUsuarios;

@Service
public class UsuariosServices {
	private static final Logger logger = LoggerFactory.getLogger(UsuariosServices.class);
	
	@Autowired
	IUsuarios iusuarios;
	
	public String login(UsuariosEntity usuario, Map<String, Object> response) {
		try {
			UsuariosEntity usuarioEntity = iusuarios.findUserByUsername(usuario.getUsername());
			if (usuarioEntity != null && usuarioEntity.getClave().equals(usuario.getClave())) {
				response.put("usuario", usuarioEntity);
			} else {
				response.put("usuario", null);
			}
		} catch (Exception e) {
			logger.error("Exception: {}", e);
		}
		return new Gson().toJson(response) ;
	}
	
}
