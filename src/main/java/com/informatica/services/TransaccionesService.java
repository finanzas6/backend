package com.informatica.services;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.TransaccionesEntity;
import com.informatica.Repositories.interfaces.ICuentas;
import com.informatica.Repositories.interfaces.ITransacciones;
import com.informatica.enums.TipoTransaccionEnum;
import com.informatica.models.TransResponse;

@Service
public class TransaccionesService {

	private static final Logger logger = LoggerFactory.getLogger(TransaccionesService.class);
	
	@Autowired
	ITransacciones iTransacciones;
	@Autowired
	ICuentas iCuentas;
	@Autowired
	com.informatica.Repositories.interfaces.ITransaccionResponse iTransaccionResponse;
	
	public void create(TransaccionesEntity transaccion, Map<String, Object> response) {
		try {
			if (validateData(transaccion) && CuentaExists(transaccion.getCuentaId())) {
				iTransacciones.save(transaccion);
				
				if (transaccion.getTipoTransaccionId() == TipoTransaccionEnum.GASTO.getId()) {
					iCuentas.updateGastosById(transaccion.getCuentaId(), transaccion.getTotal());
				} else if (transaccion.getTipoTransaccionId() == TipoTransaccionEnum.INGRESO.getId()) {
					iCuentas.updateIngresosById(transaccion.getCuentaId(), transaccion.getTotal());
				} else {
					logger.warn("tipo de transaccion invalida");
				}
					
				response.put("resultado", "transaccion insertada correctamente");
			} else {
				response.put("error", "Datos incorrectos");
				logger.info("Datos incorrectos");
			}
		} catch (Exception e) {
			logger.error("Exception: {}", e);
		}
	}

	public String getTransaccionesByUsuario(long usuarioId, Map<String, Object> response) {
		try {
			List<TransResponse> transacciones = iTransaccionResponse.findTransaccionesByUsuarioId(usuarioId);
			if (transacciones != null && !transacciones.isEmpty()) {
				response.put("transacciones", transacciones);
			} else {
				response.put("transacciones", null);
			}
		} catch (Exception e) {
			logger.error("Exception: {}", e);
		}
		return new Gson().toJson(response) ;
		
	}
	
	private boolean validateData(TransaccionesEntity transaccion) {
		return transaccion.getTipoTransaccionId() > 0 && transaccion.getCuentaId() > 0
				&& transaccion.getTotal() > 0 && transaccion.getMedioId() > 0
				&& transaccion.getCategoriaId() > 0;
	}
	
	private boolean CuentaExists(long id) {
		return iCuentas.totalCuentasByCuentaId(id) > 0;
	}

}
