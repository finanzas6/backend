package com.informatica.services;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.informatica.Repositories.entities.CategoriasEntity;
import com.informatica.Repositories.entities.CategoriasUsuarios;
import com.informatica.Repositories.interfaces.ICategorias;
import com.informatica.Repositories.interfaces.ICategoriasResponse;
import com.informatica.Repositories.interfaces.ICategoriasUsuarios;
import com.informatica.Repositories.interfaces.IUsuarios;
import com.informatica.enums.TipoTransaccionEnum;
import com.informatica.models.CategoriaRequest;
import com.informatica.models.CategoriasResponse;
import com.informatica.models.DeleteRequest;

@Service
public class CategoriasService {
	
	private static final Logger logger = LoggerFactory.getLogger(CategoriasService.class);
	
	@Autowired
	ICategorias iCategorias;
	@Autowired
	ICategoriasUsuarios iCategoriasUsuarios;
	@Autowired
	IUsuarios iUsuarios;
	@Autowired
	ICategoriasResponse iCategoriasResponse;
	
	
	public void create(CategoriaRequest categoria, Map<String, Object> response) {
		try {
			if (validateData(categoria)) {
				
				CategoriasEntity categoriaEntity = iCategorias.findCategoriaByDescripcion(categoria.getDescripcion());
				
				if (categoriaEntity == null) {
					CategoriasEntity CategoriasEntityTmp = new CategoriasEntity();
					CategoriasEntityTmp.setId(0);
					CategoriasEntityTmp.setTipoTransaccionId(categoria.getTipoTransaccionId());
					CategoriasEntityTmp.setDescripcion(categoria.getDescripcion());
					iCategorias.save(CategoriasEntityTmp);
					categoriaEntity = iCategorias.findCategoriaByDescripcion(categoria.getDescripcion());
				} 
				
				CategoriasUsuarios categoriasUsuarios = new CategoriasUsuarios();
				categoriasUsuarios.setId(0);
				categoriasUsuarios.setCategoriaId(categoriaEntity.getId());
				categoriasUsuarios.setUsuarioId(categoria.getUsuarioId());
				iCategoriasUsuarios.save(categoriasUsuarios);
				
				response.put("resultado", "categoria insertada correctamente");
			} else {
				response.put("error", "Datos incorrectos");
				logger.info("Datos incorrectos");
			}
		} catch (Exception e) {
			response.put("error", "Error interno");
			logger.error("Exception: {}", e);
		}
	}

	private boolean validateData(CategoriaRequest categoria) {
		return (categoria.getTipoTransaccionId() == TipoTransaccionEnum.GASTO.getId() || categoria.getTipoTransaccionId() == TipoTransaccionEnum.INGRESO.getId())
				&& categoria.getDescripcion() != null && !categoria.getDescripcion().isEmpty() && categoria.getUsuarioId() > 0 && userExists(categoria.getUsuarioId());
	}
	
	private boolean userExists(long id) {
		return iUsuarios.totalUsuariosById(id) > 0;
	}

	public void update(CategoriasUsuarios categoriasUsuarios, Map<String, Object> response) {
		try {
			if (categoriasUsuarios.getCategoriaId() > 0 && categoriasUsuarios.getUsuarioId() > 0) {
				iCategoriasUsuarios.save(categoriasUsuarios);
				response.put("resultado", "categoria actualizada correctamente");
			}else {
				response.put("error", "Datos incorrectos");
				logger.info("Datos incorrectos");
			}
		} catch (Exception e) {
			response.put("error", "Error interno");
			logger.error("Exception: {}", e);
		}
	}
	
	public void delete(DeleteRequest deleteRequest , Map<String, Object> response) {
		try {
			if (deleteRequest.getCategoria() != null && deleteRequest.getUsuarioId() > 0 && deleteRequest.getTipo() != null) {
				
				long id = iCategorias.getIdCategoriUsuario(deleteRequest.getCategoria(), deleteRequest.getUsuarioId(), deleteRequest.getTipo() );
				iCategoriasUsuarios.deleteById(id);
				
				response.put("resultado", "categoria eliminada correctamente");
			}else {
				response.put("error", "Datos incorrectos");
				logger.info("Datos incorrectos");
			}
		} catch (Exception e) {
			response.put("error", "Error interno");
			logger.error("Exception: {}", e);
		}
	}

	public String getCategorias(int usuarioId, Map<String, Object> response) {
		try {
			List<CategoriasResponse> categorias = iCategoriasResponse.findCategoriasByUsuarioId(usuarioId);
			if (categorias != null) {
				response.put("categorias", categorias);
			} else {
				response.put("categorias", null);
			}
		} catch (Exception e) {
			logger.error("Exception: {}", e);
		}
		return new Gson().toJson(response) ;
		
	}

}
