package com.informatica.models;

import com.google.gson.Gson;

public class DeleteRequest {
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(long usuarioId) {
		this.usuarioId = usuarioId;
	}

	private String tipo;
	private String categoria;
	private long usuarioId;
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
