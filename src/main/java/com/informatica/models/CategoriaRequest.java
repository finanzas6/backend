package com.informatica.models;

import com.google.gson.Gson;

public class CategoriaRequest {
	
	private long tipoTransaccionId;
	private String descripcion;
	private long usuarioId;
	
	public long getTipoTransaccionId() {
		return tipoTransaccionId;
	}
	public void setTipoTransaccionId(long tipoTransaccionId) {
		this.tipoTransaccionId = tipoTransaccionId;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public long getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(long usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
	
}
