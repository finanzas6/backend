package com.informatica.models;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.google.gson.Gson;

@Entity
public class CategoriasResponse {
	
	@Id
	private long id;
	private String descripcion;
	private String nombre;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
	
}
