package com.informatica.enums;

public enum TipoTransaccionEnum {

	INGRESO(1),
	GASTO(2);
	
	private int id;

	private TipoTransaccionEnum(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
	public static TipoTransaccionEnum getEnumByName(String name){
		for (TipoTransaccionEnum type : TipoTransaccionEnum.values()) {
			if (type.toString().equalsIgnoreCase(name)) {
				return type;
			}
		}
		return null;
	}
	
	public static TipoTransaccionEnum getEnumById(int id){
		for (TipoTransaccionEnum type : TipoTransaccionEnum.values()) {
			if (type.getId() == id) {
				return type;
			}
		}
		return null;
	}
}
