package com.informatica.Repositories.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.gson.Gson;

@Entity(name = "categoriasUsuarios")
public class CategoriasUsuarios {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long usuarioId;
	private long categoriaId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(long usuarioId) {
		this.usuarioId = usuarioId;
	}
	public long getCategoriaId() {
		return categoriaId;
	}
	public void setCategoriaId(long categoriaId) {
		this.categoriaId = categoriaId;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
