package com.informatica.Repositories.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.gson.Gson;

@Entity(name = "cuentas")
public class CuentasEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long usuarioId;
	private long totalIngresos;
	private long totalGastos;
	private String descripcion;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(long usuarioId) {
		this.usuarioId = usuarioId;
	}
	public long getTotalIngresos() {
		return totalIngresos;
	}
	public void setTotalIngresos(long totalIngresos) {
		this.totalIngresos = totalIngresos;
	}
	public long getTotalGastos() {
		return totalGastos;
	}
	public void setTotalGastos(long totalGastos) {
		this.totalGastos = totalGastos;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

}
