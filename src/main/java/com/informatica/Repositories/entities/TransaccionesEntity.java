package com.informatica.Repositories.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.gson.Gson;

@Entity(name = "transacciones")
public class TransaccionesEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long tipoTransaccionId;
	private long cuentaId;
	private long total;
	private Date fecha;
	private long medioId;
	private long categoriaId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTipoTransaccionId() {
		return tipoTransaccionId;
	}
	public void setTipoTransaccionId(long tipoTransaccionId) {
		this.tipoTransaccionId = tipoTransaccionId;
	}
	public long getCuentaId() {
		return cuentaId;
	}
	public void setCuentaId(long cuentaId) {
		this.cuentaId = cuentaId;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public long getMedioId() {
		return medioId;
	}
	public void setMedioId(long medioId) {
		this.medioId = medioId;
	}
	public long getCategoriaId() {
		return categoriaId;
	}
	public void setCategoriaId(long categoriaId) {
		this.categoriaId = categoriaId;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
