package com.informatica.Repositories.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.gson.Gson;

@Entity(name = "categorias")
public class CategoriasEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long tipoTransaccionId;
	private String descripcion;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTipoTransaccionId() {
		return tipoTransaccionId;
	}
	public void setTipoTransaccionId(long tipoTransaccionId) {
		this.tipoTransaccionId = tipoTransaccionId;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
	
}
