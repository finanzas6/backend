package com.informatica.Repositories.interfaces;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.informatica.Repositories.entities.UsuariosEntity;

public interface IUsuarios extends CrudRepository<UsuariosEntity, Long> {
	
	@Query(value = "SELECT * FROM finanzas.usuarios WHERE username LIKE :username ;", nativeQuery= true)
	UsuariosEntity findUserByUsername(@Param("username") String username);
	
	@Query(value = "SELECT COUNT(*) AS total FROM finanzas.usuarios WHERE id = :id ;", nativeQuery= true)
	int totalUsuariosById(@Param("id") long id);

}
