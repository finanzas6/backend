package com.informatica.Repositories.interfaces;

import java.util.List;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.informatica.Repositories.entities.CuentasEntity;

public interface ICuentas extends CrudRepository<CuentasEntity, Long>{
	
	@Query(value = "SELECT * FROM finanzas.cuentas WHERE usuarioId = :usuarioId ;", nativeQuery= true)
	List<CuentasEntity> findCuentasByUsuarioId(@Param("usuarioId") long usuarioId);

	@Query(value = "SELECT COUNT(*) AS total FROM finanzas.cuentas WHERE id = :id ;", nativeQuery= true)
	int totalCuentasByCuentaId(@Param("id") long id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE finanzas.cuentas SET totalIngresos = totalIngresos + :total WHERE id = :cuentaId ; ", nativeQuery = true)
	int updateIngresosById(@Param("cuentaId") long cuentaId, @Param("total") long total);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE finanzas.cuentas SET totalGastos = totalGastos + :total WHERE id = :cuentaId ; ", nativeQuery = true)
	int updateGastosById(@Param("cuentaId") long cuentaId, @Param("total") long total);
}
