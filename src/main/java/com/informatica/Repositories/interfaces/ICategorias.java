package com.informatica.Repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.informatica.Repositories.entities.CategoriasEntity;

public interface ICategorias extends CrudRepository<CategoriasEntity, Long>{
	
	@Query(value = "SELECT * FROM  finanzas.categorias WHERE descripcion  LIKE :descripcion ;", nativeQuery= true)
	CategoriasEntity findCategoriaByDescripcion(@Param("descripcion") String descripcion);
	
	@Query(value = "SELECT cu.id AS id FROM finanzas.categorias c \n" + 
			"INNER JOIN finanzas.categoriasUsuarios cu ON c.id = cu.categoriaId\n" + 
			"INNER JOIN finanzas.tipoTransacciones tt ON tt.id = c.tipoTransaccionId \n" + 
			"INNER JOIN finanzas.usuarios u ON u.id = cu.usuarioId \n" + 
			"WHERE cu.usuarioId = :usuarioId AND c.descripcion LIKE :categoria AND tt.descripcion LIKE :tipo  ;", nativeQuery= true)
	long getIdCategoriUsuario(
			@Param("categoria") String categoria,
			@Param("usuarioId") long usuarioId,
			@Param("tipo") String tipo
			);
	
	@Query(value = "SELECT * FROM finanzas.categorias c WHERE c.tipoTransaccionId = :tipo ; ", nativeQuery= true)
	List<CategoriasEntity> getCategoriasByTipo(@Param("tipo") long tipo);

}
