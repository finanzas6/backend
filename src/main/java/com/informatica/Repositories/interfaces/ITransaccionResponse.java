package com.informatica.Repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.informatica.models.TransResponse;


public interface ITransaccionResponse extends CrudRepository<TransResponse, Long>{
	
	@Query(value = "SELECT \n" + 
			"	t.id AS id, \n" + 
			"	tt.descripcion AS tipoTransaccion, \n" + 
			"	t.cuentaId AS cuentaId, \n" + 
			"	t.total AS total, \n" + 
			"	t.fecha AS fecha, \n" + 
			"	mp.descripcion AS medioPago, \n" + 
			"	c2.descripcion AS categoria \n" + 
			"FROM finanzas.transacciones t \n" + 
			"INNER JOIN finanzas.cuentas c ON t.cuentaId = c.id\n" + 
			"INNER JOIN finanzas.tipoTransacciones tt ON tt.id = t.tipoTransaccionId \n" + 
			"INNER JOIN finanzas.medioPago mp ON mp.id = t.medioId \n" + 
			"INNER JOIN finanzas.categorias c2 ON c2.id = t.categoriaId \n" + 
			"WHERE c.usuarioId = :usuarioId ;", nativeQuery= true)
	List<TransResponse> findTransaccionesByUsuarioId(@Param("usuarioId") long usuarioId);

}
