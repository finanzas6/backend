package com.informatica.Repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.informatica.models.CategoriasResponse;

public interface ICategoriasResponse extends CrudRepository<CategoriasResponse, Long>{

	@Query(value = "SELECT cu.id AS id, c.descripcion AS descripcion, tt.descripcion AS nombre FROM finanzas.categorias c \n" + 
			"INNER JOIN finanzas.categoriasUsuarios cu ON c.id = cu.categoriaId\n" + 
			"INNER JOIN finanzas.tipoTransacciones tt ON tt.id = c.tipoTransaccionId \n" + 
			"INNER JOIN finanzas.usuarios u ON u.id = cu.usuarioId \n" + 
			"WHERE cu.usuarioId = :usuarioId ;  ", nativeQuery = true)
	List<CategoriasResponse> findCategoriasByUsuarioId(@Param("usuarioId") long usuarioId);
}
