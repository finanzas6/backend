package com.informatica.Repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.informatica.Repositories.entities.TransaccionesEntity;

public interface ITransacciones extends CrudRepository<TransaccionesEntity, Long>{
	
	@Query(value = "SELECT \n" + 
			"	tt.descripcion AS transaccion,\n" + 
			"	SUM(t.total) AS total \n" + 
			"FROM \n" + 
			"	finanzas.transacciones t\n" + 
			"	INNER JOIN finanzas.cuentas cc ON cc.id = t.cuentaId \n" + 
			"	INNER JOIN finanzas.usuarios u ON u.id = cc.usuarioId \n" + 
			"	INNER JOIN finanzas.tipoTransacciones tt ON tt.id = t.tipoTransaccionId \n" + 
			"WHERE u.id = :usuarioId \n" + 
			"GROUP BY 1; ", nativeQuery= true)
	List<Object[]> getTransaccionesByTipo(@Param("usuarioId") long usuarioId);
	
	@Query(value = "SELECT \n" + 
			"	c.descripcion AS categoria,\n" + 
			"	SUM(t.total) AS total \n" + 
			"FROM \n" + 
			"	finanzas.transacciones t\n" + 
			"	INNER JOIN finanzas.cuentas cc ON cc.id = t.cuentaId \n" + 
			"	INNER JOIN finanzas.usuarios u ON u.id = cc.usuarioId \n" + 
			"	INNER JOIN finanzas.categorias c ON c.id = t.categoriaId \n" + 
			"WHERE u.id = :usuarioId \n" + 
			"GROUP BY 1; ", nativeQuery= true)
	List<Object[]> getTransaccionesByCategoria(@Param("usuarioId") long usuarioId);
}
