package com.informatica.Repositories.interfaces;

import org.springframework.data.repository.CrudRepository;

import com.informatica.Repositories.entities.TipoTransaccionesEntity;

public interface ITipoTransacciones extends CrudRepository<TipoTransaccionesEntity, Long>{

}
