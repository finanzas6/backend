package com.informatica.Repositories.interfaces;

import org.springframework.data.repository.CrudRepository;

import com.informatica.Repositories.entities.CategoriasUsuarios;

public interface ICategoriasUsuarios extends CrudRepository<CategoriasUsuarios, Long>{

}
