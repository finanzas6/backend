package com.informatica.Repositories.interfaces;

import org.springframework.data.repository.CrudRepository;

import com.informatica.Repositories.entities.MedioPagoEntity;

public interface IMedioPagos extends CrudRepository<MedioPagoEntity, Long>{

}
