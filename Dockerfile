FROM java:8

CMD ["mkdir", "/home/app/"]
COPY target/cliente-0.0.1-SNAPSHOT.jar /home/app/

CMD ["mkdir", "/config/"]
COPY src/main/resources/* /config/

EXPOSE 8081